import Foundation

let droidController = PlaygroundPageDroidController.shared

public func moveForward(speed: Int = MovePreset.maxSpeed, forSeconds seconds: TimeInterval = 2) {
  waitUntil { done in
    droidController.send(.moveForward(speed: speed, duration: seconds)) { response in
      switch response {
      case .complete:
        stop()
        done()
      }
    }
//    delay(forSeconds)
//    stop()
  }
}

public func moveBackward(speed: Int = MovePreset.maxSpeed, forSeconds seconds: TimeInterval = 2) {
  waitUntil { done in
    droidController.send(.moveBackward(speed: speed, duration: seconds)) { response in
      switch response {
      case .complete:
        stop()
        done()
      }
    }
  }
}

public func setTurnDirection(_ direction: TurnDirection, angle: Int = MovePreset.maxAngle) {
  waitUntil { done in
    droidController.send(.setTurnDirection(direction, angle: angle)) { response in
      switch response {
      case .complete:
        done()
      }
    }
  }
}

public func stop() {
  waitUntil { done in
    droidController.send(.stop) { response in
      switch response {
      case .complete:
        done()
      }
    }
  }
  setTurnDirection(.center)
}

public func playSound(_ sound: SoundPreset = .attitude) {
  waitUntil { done in
    droidController.send(.playSound(sound)) { response in
      switch response {
      case .complete:
        done()
      }
    }
  }
}

private func waitUntil(executing body: (_ done: @escaping () -> Void) -> Void) {
  let runLoop = CFRunLoopGetCurrent()
  var isComplete = false

  func done() {
    isComplete = true

    CFRunLoopPerformBlock(runLoop, CFRunLoopMode.defaultMode.rawValue) {
      CFRunLoopStop(runLoop)
    }
    CFRunLoopWakeUp(runLoop)
  }

  body(done)

  while !isComplete {
    CFRunLoopRun()
  }
}

func delay(_ delay:Double, closure:@escaping ()->()) {
    let when = DispatchTime.now() + delay
    DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
}
