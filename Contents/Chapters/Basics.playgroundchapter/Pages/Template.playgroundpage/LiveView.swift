import PlaygroundSupport

let droidController = LiveViewDroidController()

let container = LiveViewContainer.instantiate(from: .main)
container.droidController = droidController
container.enabledStatusIndicators = .all
container.installConnectionView(for: droidController)
container.installSafeAreaLayoutConstraints()

PlaygroundPage.current.liveView = container
PlaygroundPage.current.needsIndefiniteExecution = true
