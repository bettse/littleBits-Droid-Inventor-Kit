import UIKit

@objc(EmbedLiveViewContentSegue) public final class EmbedLiveViewContentSegue: UIStoryboardSegue {
  override public func perform() {
    let source = self.source as! LiveViewContainer

    source.addChildViewController(destination)
    source.contentView.addSubview(destination.view)

    destination.view.preservesSuperviewLayoutMargins = true
    destination.view.translatesAutoresizingMaskIntoConstraints = false

    NSLayoutConstraint.activate([
      source.contentView.topAnchor.constraint(equalTo: destination.view.topAnchor),
      source.contentView.trailingAnchor.constraint(equalTo: destination.view.trailingAnchor),
      source.contentView.bottomAnchor.constraint(equalTo: destination.view.bottomAnchor),
      source.contentView.leadingAnchor.constraint(equalTo: destination.view.leadingAnchor),
    ])

    destination.didMove(toParentViewController: source)
  }
}
