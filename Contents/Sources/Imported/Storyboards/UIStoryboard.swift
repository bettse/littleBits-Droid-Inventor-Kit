import UIKit

extension UIStoryboard {
  public static let main = UIStoryboard(name: "Main", bundle: Bundle(for: Handle.self))
}

private final class Handle {}
