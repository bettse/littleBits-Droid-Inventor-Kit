import CoreBluetooth

private let BLEScanTimeout: TimeInterval = 5 // seconds
private let RSSILimit = -50.0

protocol BLEManagerDelegate: class {
    func managerDidTimeoutScan()
    func managerDidFail(forState state: CBManagerState)
    func managerDidFind(device: BLEDevice)
    func managerDidDisconnect()
}

final class BLEManager: NSObject {
    let serviceUUID: CBUUID
    let characteristicUUID: CBUUID
    let central: CBCentralManager
    var savedPeripheralUUID: UUID?
    var scanTimeoutTimer: Timer?
    var connectedPeripheral: CBPeripheral?
    var connectedDevice: BLEDevice?
    weak var delegate: BLEManagerDelegate?

    init(serviceUUID: CBUUID, characteristicUUID: CBUUID, savedPeripheralUUID: UUID? = .none) {
        self.serviceUUID = serviceUUID
        self.characteristicUUID = characteristicUUID
        self.savedPeripheralUUID = savedPeripheralUUID

        let queue = DispatchQueue(label: "cc.littlebits.discovery")
        central = CBCentralManager(delegate: .none, queue: queue)
        super.init()
        central.delegate = self
    }

    fileprivate func connectToSavedPeripheral() {
        guard let savedPeripheralUUID = savedPeripheralUUID,
            let peripheral = central.retrievePeripherals(withIdentifiers: [savedPeripheralUUID]).first ??
                central.retrieveConnectedPeripherals(withServices: [serviceUUID]).first
            else { return }

        connectedPeripheral = peripheral
        central.connect(peripheral)
    }

    public func scan() {
        central.scanForPeripherals(withServices: [serviceUUID], options: [CBCentralManagerScanOptionAllowDuplicatesKey: true])

        scanTimeoutTimer = Timer.scheduledTimer(withTimeInterval: BLEScanTimeout, repeats: false) { [weak self] _ in
            self?.stopScan()
            self?.delegate?.managerDidTimeoutScan()
        }
    }

    public func stopScan() {
        scanTimeoutTimer?.invalidate()
        scanTimeoutTimer = .none
        central.stopScan()
    }
}

extension BLEManager: CBCentralManagerDelegate {
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .poweredOn: connectToSavedPeripheral()
        default: delegate?.managerDidFail(forState: central.state)
        }
    }

    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        stopScan()
        peripheral.delegate = self
        peripheral.discoverServices([serviceUUID])
    }

    func centralManager(_ central: CBCentralManager, willRestoreState dict: [String : Any]) {

    }

    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print(error ?? "Failed to connect to peripheral")

    }

    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        connectedPeripheral = .none
        connectedDevice = .none
        delegate?.managerDidDisconnect()

        if let error = error as? CBError, error.code == CBError.connectionTimeout {
            connectToSavedPeripheral()
        } else {
            if let error = error {
                print(error)
            } else {
                connectToSavedPeripheral()
            }
        }
    }

    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        guard RSSI.doubleValue >= RSSILimit, peripheral.state == .disconnected else { return }
        connectedPeripheral = peripheral
        central.connect(peripheral)
        stopScan()
    }
}

extension BLEManager: CBPeripheralDelegate {
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        guard peripheral == connectedPeripheral else { return }
        peripheral.services?.forEach {
            peripheral.discoverCharacteristics([characteristicUUID], for: $0)
        }
    }

    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        let disconnect: () -> Void = { [weak central] in
          central?.cancelPeripheralConnection(peripheral)
        }

        guard peripheral == connectedPeripheral, service.uuid == serviceUUID else { return }
        guard let characteristic = service.characteristics?.first(where: { $0.uuid == characteristicUUID }) else {
            disconnect()
            return
        }

        peripheral.setNotifyValue(true, for: characteristic)

        connectedDevice = BLEDevice(
          peripheral: peripheral,
          characteristic: characteristic,
          disconnectHandler: disconnect)

        delegate?.managerDidFind(device: connectedDevice!)
    }

    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        connectedDevice?.setValue(characteristic.value ?? Data())
    }

    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        connectedDevice?.didWriteValue()
    }
}
