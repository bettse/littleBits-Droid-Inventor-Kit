public struct Control {
    public let controlType: ControlType
    public let output: Output

    public init(controlType: ControlType, output: Output) {
        self.controlType = controlType
        self.output = output
    }

    public enum ControlType: Int {
        case joystick           // 0
        case tilt               // 1
        case button             // 2
        case soundPresets       // 3
        case sliderInput        // 4
        case switchInput        // 5
        case shake              // 6
        case swing              // 7
        case alert              // 8
        case camera             // 9
        case counter            // 10
        case readout            // 11
        case bargraph           // 12
        case headSpin           // 13
        case armWave            // 14
        case guardControl       // 15
        case forceDrive         // 16
        case selfNavDrive       // 17
        case selfNavResponse    // 18

        public var category: Category {
            switch self {
            case .joystick:
                return .drive
            case .tilt:
                return .drive
            case .button:
                return .input
            case .soundPresets:
                return .input
            case .sliderInput:
                return .input
            case .switchInput:
                return .input
            case .shake:
                return .input
            case .swing:
                return .input
            case .alert:
                return .response
            case .camera:
                return .response
            case .counter:
                return .response
            case .readout:
                return .response
            case .bargraph:
                return .response
            case .headSpin:
                return .input
            case .armWave:
                return .response
            case .guardControl:
                return .response
            case .forceDrive:
                return .response
            case .selfNavDrive:
                return .drive
            case .selfNavResponse:
                return .response
            }
        }

        static let allValues = [joystick, tilt, button, soundPresets, sliderInput, switchInput, shake, swing, alert, camera, counter, readout, bargraph, headSpin, armWave, guardControl, forceDrive, selfNavDrive, selfNavResponse]
    }

    public enum Category: Int {
        case drive
        case input
        case response

        var controlsForCategory: [ControlType] {
            switch self {
            case .drive:
                return ControlType.allValues.filter { $0.category == .drive }
            case .input:
                return ControlType.allValues.filter { $0.category == .input }
            case .response:
                return ControlType.allValues.filter { $0.category == .response }
            }
        }
    }

    public enum Output: Int {
        case out1 = 1
        case out2
        case out3

        public var output: UInt8 {
            switch self {
            case .out1:
                return 0x00
            case .out2:
                return 0x01
            case .out3:
                return 0x02
            }
        }
    }
}
