public enum LEDColor: Int {
    case blue
    case green
    case orange
    case purple
    case red
    case white
    case yellow

    public var values: [UInt8] {
        switch self {
        case .blue:
            return [0, 0, 6]
        case .green:
            return [0, 6, 0]
        case .orange:
            return [12, 1, 0]
        case .purple:
            return [3, 0, 6]
        case .red:
            return [6, 0, 0]
        case .white:
            return [6, 5, 4]
        case .yellow:
            return [6, 3, 0]
        }
    }
}
