import CoreBluetooth
import CoreGraphics

private let starServiceUUID = CBUUID(string: "D9D9E9E0-AA4E-4797-8151-CB41CEDAF2AD")
private let dataCharacteristicUUID = CBUUID(string: "D9D9E9E1-AA4E-4797-8151-CB41CEDAF2AD")
private let savedStarDeviceUUIDKey = "savedStarDeviceUUID"

public protocol BLEConnectionManagerDelegate: class {
    func didUpdateState()
    func didUpdateValue(value: Int)
}

public final class BLEConnectionManager {
    public enum State {
        case searching
        case timeout
        case failed
        case connected
        case disconnected
    }

    let manager: BLEManager
    private var messageQueue: [(Data, Progress)] = []
    private var queueProgress = Progress()

    public weak var delegate: BLEConnectionManagerDelegate?
    public var defaultLedColor: LEDColor = .white

    public fileprivate(set) var state = State.disconnected {
        didSet {
            guard state != oldValue else { return }
            delegate?.didUpdateState()
        }
    }

    var device: BLEDevice? {
        didSet {
            guard let device = device else { return }
            UserDefaults.standard.set(device.peripheral.identifier.uuidString, forKey: savedStarDeviceUUIDKey)
            device.observe = { [weak self] in self?.deviceDidRespond(value: $0) }
        }
    }

    public init() {
        let uuid = UserDefaults.standard.string(forKey: savedStarDeviceUUIDKey).flatMap(UUID.init(uuidString:))
        manager = BLEManager(serviceUUID: starServiceUUID,
                             characteristicUUID: dataCharacteristicUUID,
                             savedPeripheralUUID: uuid)
        manager.delegate = self
    }

    public var isBluetoothPoweredOn: Bool {
        return manager.central.state == .poweredOn
    }

    public var isConnected: Bool {
        return device != nil
    }

    public func forgetSavedDevice() {
        manager.savedPeripheralUUID = .none
        UserDefaults.standard.removeObject(forKey: savedStarDeviceUUIDKey)
        device?.disconnect()
    }

    public func scan() {
        state = .searching
        manager.scan()
    }

    public func enqueue(_ request: BLERequest) {
        let data = request.encode()
        queueProgress.totalUnitCount += Int64(data.count)
        let childProgress = Progress(totalUnitCount: Int64(data.count))
        queueProgress.addChild(childProgress, withPendingUnitCount: Int64(data.count))
        messageQueue.append((data, childProgress))
    }

    @discardableResult
    public func flushQueue() -> Progress {
        guard !messageQueue.isEmpty else { queueProgress = Progress(); return queueProgress }
        let message = messageQueue.first!
        device?.send(data: message.0, with: message.1)
        return queueProgress
    }

    public func send(_ request: BLERequest) {
        let data = request.encode()
        device?.send(data: data)
    }

    func deviceDidRespond(value: Data) {
        switch BLEDeviceResponse(data: value) {
        case .success:
            if !messageQueue.isEmpty {
                messageQueue.removeFirst()
                flushQueue()
        }

        case .failure(let s): print(s) // run next command? try again? pure queue?
        case .value(let v): delegate?.didUpdateValue(value: v)
        }
    }
    
    func resetOutputs() {
        var resetRequests = BLERequest.voltageResetRequests
        let ledResetRequest = BLERequest.makeLedColorRequest(color: defaultLedColor)
        resetRequests.append(ledResetRequest)
        
        resetRequests.forEach { send($0) }
    }

    deinit {
        device?.disconnect()
    }
}

extension BLEConnectionManager: BLEManagerDelegate {
    func managerDidTimeoutScan() {
        state = .timeout
        print("timeout")
    }

    func managerDidFail(forState state: CBManagerState) {
        self.state = .failed
        print(state)
    }

    func managerDidDisconnect() {
        state = .disconnected
        device = .none
    }

    func managerDidFind(device: BLEDevice) {
        self.device = device
        state = .connected
        resetOutputs()
    }
}
