import CoreBluetooth

private let maxSendableData = 20

public final class BLEDevice {
    public let peripheral: CBPeripheral
    public let characteristic: CBCharacteristic
    var observe: ((Data) -> Void)?
    var isRecievingADC = false

    public let disconnect: () -> Void

    private var sendBuffer = Data()
    private weak var progress: Progress?
    private var valueBuffer = Data()

    private(set) var value = Data() {
        didSet {
            observe?(value)
        }
    }

    public init(peripheral: CBPeripheral, characteristic: CBCharacteristic, disconnectHandler: @escaping () -> Void) {
        self.peripheral = peripheral
        self.characteristic = characteristic
        self.disconnect = disconnectHandler
    }

    public func send(data: Data) {
        if data.count > maxSendableData {
            sendBuffer = data
            sendChunk()
        } else {
            // This is in place to try and drop bad data when recieving ADC updates while sending other commands
            if data.first.map({ $0 >> 1 }) == UInt8(BLECommand.CMD_ADC_UPDATE.rawValue) {
                isRecievingADC = (data.subdata(in: 2..<6).reduce(0, +) > 0)
            }
            peripheral.writeValue(data, for: characteristic, type: .withoutResponse)
        }
    }

    func send(data: Data, with progress: Progress) {
        sendBuffer = data
        self.progress = progress
        sendChunk()
    }

    private func sendChunk() {
        guard !sendBuffer.isEmpty else { return }

        let length = min(maxSendableData, sendBuffer.count)
        let data = sendBuffer.subdata(in: 0..<length)
        peripheral.writeValue(data, for: characteristic, type: .withResponse)
    }

    public func didWriteValue() {
        let length = min(maxSendableData, sendBuffer.count)
        sendBuffer.removeFirst(length)
        progress?.completedUnitCount += Int64(length)
        sendChunk()
    }

    public func setValue(_ value: Data) {
        valueBuffer.append(value)

        // This is in place to try and drop bad data when recieving ADC updates while sending other commands
        if isRecievingADC {
            if let first = valueBuffer.first,
                let second = valueBuffer[safe: 1],
                (first, second) != (0xFE, 0x02) && (first, second) != (0x02, 0x02) {
                valueBuffer.removeFirst()
                setValue(Data())
                return
            }
        }

        guard let lengthMSB = valueBuffer.first.map({ Int($0 & 0x01) << 8 }),
            let length = valueBuffer[safe: 1].map({ lengthMSB + Int($0) }).map({ $0 + 4 }),
            valueBuffer.count >= length else { return }

        self.value = valueBuffer.subdata(in: 0..<length)
        valueBuffer.removeSubrange(0..<length)

        if valueBuffer.count > 0 {
            setValue(Data())
        }
    }
}
