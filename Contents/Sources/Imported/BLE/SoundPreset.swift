public enum SoundPreset: UInt8 {
    case analyzing
    case attitude
    case blushing
    case concentrating
    case cooing
    case doingThings
    case fart
    case flirt
    case happy
    case ideating
    case inquiry
    case laugh
    case no
    case oldBootup
    case processing
    case scream
    case shutdown
    case sing
    case trumpet
    case whining
    case whistle
    case yes
    case analyzingOldBootup

    public static let allValues = [analyzing, attitude, blushing, concentrating, cooing, doingThings, fart, flirt, happy, ideating, inquiry, laugh, no, oldBootup, processing, scream, shutdown, sing, trumpet, whining, whistle, yes]
}
