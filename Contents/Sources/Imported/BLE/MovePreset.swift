public enum MovePreset {
    case forward
    case forwardLeft
    case forwardRight
    case backward
    case backwardLeft
    case backwardRight
    case stop

    public var dcOutputValue: UInt8 {
        switch self {
        case .forward:
            return 255
        case .forwardLeft:
            return 255
        case .forwardRight:
            return 255
        case .backward:
            return 0
        case .backwardLeft:
            return 0
        case .backwardRight:
            return 0
        case .stop:
            return 135
        }
    }

    public var servoOutputValue: UInt8 {
        switch self {
        case .forward:
            return 127
        case .forwardLeft:
            return 195
        case .forwardRight:
            return 60
        case .backward:
            return 127
        case .backwardLeft:
            return 195
        case .backwardRight:
            return 60
        case .stop:
            return 127
        }
    }

    public static let allValues = [forward, forwardLeft, forwardRight, backward, backwardLeft, backwardRight, stop]
}
