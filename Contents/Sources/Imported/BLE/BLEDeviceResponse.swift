import Foundation

enum BLEDeviceResponse {
    case success
    case failure(String)
    case value(Int)

    init(data: Data) {
        guard let command = data.first.map({ Int($0 >> 1) & 0x7F }),
            let lengthMSB = data.first.map({ Int($0 & 1) << 8 }),
            let dataLength = data[safe: 1].map({ lengthMSB + Int($0) }) else {
                self = .failure("Could not parse data.")
                return
        }

        let value: Int = data.subdata(in: 2..<(2 + dataLength)).reduce(0) { $0 << 8 + Int($1) }

        switch (command, value) {
        case (0x1, 0), (0x12, _): self = .success
        case (0x1, _): print(data.pretty); self = .failure("Unknown device failure.")
        case (0x7F, let v): self = .value(v)
        case (_, _): print(data.pretty); self = .failure("Unknown device response.")
        }
    }
}
