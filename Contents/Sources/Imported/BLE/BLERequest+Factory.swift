import Foundation

extension BLERequest {
    public static var voltageResetRequests: [BLERequest] {
        // Set output default to 2.65v
        let connectionRequest = BLERequest(unsafeData: Data(bytes: [0x30, 0x02, 0x99, 0xFF, 0xA1, 0x8C]))
        let resetRequest = BLERequest(unsafeData: Data(bytes: [0x2C, 0x14, 0x00, 0x00, 0x00, 0x00, 0x00, 0x89, 0x00, 0x00, 0x01, 0x89, 0x00, 0x00, 0x02, 0x89, 0x00, 0x00, 0xC0, 0x00, 0x00, 0x00, 0x67, 0x3C]))
        return [connectionRequest, resetRequest]
    }

    public static func makeLedColorRequest(color: LEDColor) -> BLERequest {
        return makeLedColorRequest(values: color.values)
    }
}

fileprivate extension BLERequest {
    static func makeLedColorRequest(values: [UInt8]) -> BLERequest {
        return BLERequest(command: .CMD_SET_LEDS, payload: values)
    }
}
