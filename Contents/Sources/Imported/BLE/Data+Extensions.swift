import Foundation

extension Data {
    subscript(safe index: Data.Index) -> UInt8? {
        guard indices.contains(index) else { return .none }
        return self[index]
    }

    var pretty: String {
        return map { String(format: "%02hhx", $0) }.joined()
    }
}
