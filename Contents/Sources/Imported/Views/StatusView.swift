import UIKit

@objc(StatusView) class StatusView: UIView {
  enum ActiveState {
    case disabled
    case enabled
    case active

    var isEnabled: Bool {
      switch self {
      case .enabled, .active:
        return true
      case .disabled:
        return false
      }
    }
  }

  let titleLabel = UILabel()
  let statusLabel = UILabel()

  let stateIndicatorView = CircleView()

  private let contentWrapperView = UIStackView()
  let contentView = UIView()
  let contentCenterGuide = UILayoutGuide()

  var activeState: ActiveState = .disabled {
    didSet {
      update()
    }
  }

  var isEnabled: Bool {
    return activeState.isEnabled
  }

  override func awakeFromNib() {
    super.awakeFromNib()
    layout()
    configure()
    update()
  }

  func layout() {
    layoutWrapper()
    layoutContent()
  }

  private func layoutWrapper() {
    let subviews = [
      "title": titleLabel,
      "indicator": stateIndicatorView,
      "content": contentWrapperView,
    ]

    for (_, view) in subviews {
      addSubview(view)
      view.translatesAutoresizingMaskIntoConstraints = false
    }

    let titleRow = NSLayoutConstraint.constraints(
      withVisualFormat: "|-[title]-[indicator(6)]-|", metrics: nil, views: subviews)

    NSLayoutConstraint.activate(titleRow)

    NSLayoutConstraint.activate([
      titleLabel.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor),

      stateIndicatorView.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor),
      stateIndicatorView.heightAnchor.constraint(equalTo: stateIndicatorView.widthAnchor),

      contentWrapperView.topAnchor.constraint(equalTo: titleLabel.lastBaselineAnchor, constant: 16),

      contentWrapperView.leadingAnchor.constraint(equalTo: leadingAnchor),
      contentWrapperView.trailingAnchor.constraint(equalTo: trailingAnchor),
      contentWrapperView.bottomAnchor.constraint(equalTo: bottomAnchor),
    ])
  }

  private func layoutContent() {
    contentWrapperView.addArrangedSubview(contentView)
    contentView.addLayoutGuide(contentCenterGuide)

    let statusContainerView = UIView()
    statusContainerView.addSubview(statusLabel)
    statusContainerView.preservesSuperviewLayoutMargins = true
    statusContainerView.translatesAutoresizingMaskIntoConstraints = false
    contentWrapperView.addArrangedSubview(statusContainerView)

    NSLayoutConstraint.activate([
      contentCenterGuide.topAnchor.constraint(equalTo: contentView.topAnchor),
      contentCenterGuide.leadingAnchor.constraint(equalTo: contentView.centerXAnchor),
      contentCenterGuide.trailingAnchor.constraint(equalTo: contentView.centerXAnchor),
      contentCenterGuide.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),

      statusLabel.firstBaselineAnchor.constraint(equalTo: statusContainerView.layoutMarginsGuide.topAnchor, constant: 16),
      statusLabel.leadingAnchor.constraint(equalTo: statusContainerView.layoutMarginsGuide.leadingAnchor),
      statusLabel.trailingAnchor.constraint(equalTo: statusContainerView.layoutMarginsGuide.trailingAnchor),
    ])
  }

  func configure() {
    layer.cornerRadius = 2
    layer.borderWidth = 1

    contentWrapperView.alignment = .fill
    contentWrapperView.axis = .vertical
    contentWrapperView.distribution = .fillEqually
    contentWrapperView.preservesSuperviewLayoutMargins = true

    contentView.preservesSuperviewLayoutMargins = true
    contentView.translatesAutoresizingMaskIntoConstraints = false

    stateIndicatorView.layer.borderWidth = 1

    statusLabel.font = .statusDescription
    statusLabel.text = "—"
    statusLabel.textAlignment = .center
    statusLabel.translatesAutoresizingMaskIntoConstraints = false

    titleLabel.font = .statusHeader
    titleLabel.textColor = .white
    titleLabel.setContentHuggingPriority(UILayoutPriorityRequired, for: .vertical)
  }

  func update() {
    let backgroundColor: UIColor = isEnabled ? .statusActive : .clear
    let stateIndicatorBorderColor: UIColor = isEnabled ? .statusActive : .statusDisabled
    let tintColor: UIColor = isEnabled ? .white : .statusDisabled

    let borderColor: UIColor
    switch activeState {
    case .active:
      borderColor = .statusActive
    case .enabled:
      borderColor = .statusEnabled
    case .disabled:
      borderColor = .statusDisabled
    }

    layer.borderColor = borderColor.cgColor
    stateIndicatorView.backgroundColor = backgroundColor
    stateIndicatorView.layer.borderColor = stateIndicatorBorderColor.cgColor

    if !isEnabled {
      clearOverriddenTintColors()
      statusLabel.setText("—", animated: true)
    }

    setTintColor(tintColor, animated: true)
  }

  override func tintColorDidChange() {
    super.tintColorDidChange()
    titleLabel.textColor = tintColor
    statusLabel.textColor = tintColor
  }
}
