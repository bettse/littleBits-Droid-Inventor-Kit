import UIKit

@objc(TurnStatusView) final class TurnStatusView: StatusView {
  enum Status {
    case centered
    case turningLeft(angle: Int)
    case turningRight(angle: Int)
  }

  private let leftArrow = UIImageView(image: #imageLiteral(resourceName: "turn_angle_left"))
  private let centerArrow = UIImageView(image: #imageLiteral(resourceName: "turn_angle_center"))
  private let rightArrow = UIImageView(image: #imageLiteral(resourceName: "turn_angle_right"))

  var status: Status = .centered {
    didSet {
      update()
    }
  }

  override func configure() {
    super.configure()
    titleLabel.text = localizedString(forKey: "live-view.turn-angle-label", table: "LiveView")
  }

  override func layout() {
    super.layout()

    leftArrow.translatesAutoresizingMaskIntoConstraints = false
    leftArrow.installImageAspectRatioConstraint()
    contentView.addSubview(leftArrow)

    centerArrow.translatesAutoresizingMaskIntoConstraints = false
    centerArrow.installImageAspectRatioConstraint()
    contentView.addSubview(centerArrow)

    rightArrow.translatesAutoresizingMaskIntoConstraints = false
    rightArrow.installImageAspectRatioConstraint()
    contentView.addSubview(rightArrow)

    NSLayoutConstraint.activate([
      centerArrow.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
      centerArrow.bottomAnchor.constraint(equalTo: contentView.layoutMarginsGuide.bottomAnchor),
    ])

    let views = [
      "left": leftArrow,
      "center": centerArrow,
      "right": rightArrow,
    ]

    let row = NSLayoutConstraint.constraints(
      withVisualFormat: "|-(>=0)-[left]-[center]-[right]-(>=0)-|",
      options: .alignAllBottom, metrics: nil, views: views)


    NSLayoutConstraint.activate(row)
  }

  override func update() {
    leftArrow.tintColor = .statusDisabled
    centerArrow.tintColor = .statusDisabled
    rightArrow.tintColor = .statusDisabled

    let enabledArrow: UIImageView

    switch status {
    case .centered:
      enabledArrow = centerArrow
    case .turningLeft:
      enabledArrow = leftArrow
    case .turningRight:
      enabledArrow = rightArrow
    }

    enabledArrow.setTintColor(.white, animated: true)
    statusLabel.setText(localizedAngleDescription, animated: true)

    super.update()
  }

  private var localizedAngleDescription: String {
    let formatter = MeasurementFormatter()
    formatter.unitStyle = .short
    return formatter.string(from: status.angle)
  }
}

private extension TurnStatusView.Status {
  var angle: Measurement<UnitAngle> {
    let value: Double

    switch self {
    case .centered:
      value = 0
    case let .turningLeft(degrees), let .turningRight(degrees):
      value = Double(degrees)
    }

    return Measurement(value: value, unit: .degrees)
  }
}
