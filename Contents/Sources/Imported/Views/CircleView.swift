import UIKit

@objc(CircleView) public final class CircleView: UIView {
  override public init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }

  required public init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  private func setup() {
    heightAnchor.constraint(equalTo: widthAnchor, multiplier: 1).isActive = true
  }

  override public func layoutSubviews() {
    super.layoutSubviews()
    layer.cornerRadius = bounds.width / 2
  }
}
