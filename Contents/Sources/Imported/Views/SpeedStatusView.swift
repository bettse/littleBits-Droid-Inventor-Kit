import UIKit

private let heightRatios: [Double] = [5/40, 10/40, 18/40, 28/40, 40/40]

@objc(SpeedStatusView) final class SpeedStatusView: StatusView {
  private let barStackView = UIStackView()

  var speed = 0 {
    didSet {
      update()
    }
  }

  override func layout() {
    super.layout()

    contentView.addSubview(barStackView)

    NSLayoutConstraint.activate([
      barStackView.topAnchor.constraint(equalTo: contentView.topAnchor),
      barStackView.leadingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leadingAnchor),
      barStackView.trailingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor),
      barStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
    ])

    for (offset, heightRatio) in heightRatios.enumerated() {
      let width = (1 + CGFloat(offset)) * 5

      let bar = UIView()
      bar.backgroundColor = .white
      bar.layer.borderWidth = 1
      bar.translatesAutoresizingMaskIntoConstraints = false
      barStackView.addArrangedSubview(bar)

      let widthConstraint = bar.widthAnchor.constraint(equalToConstant: width)
      widthConstraint.priority = 1000

      NSLayoutConstraint.activate([
        widthConstraint,
        bar.heightAnchor.constraint(equalTo: barStackView.heightAnchor, multiplier: CGFloat(heightRatio)),
      ])
    }
  }

  override func configure() {
    super.configure()

    barStackView.alignment = .bottom
    barStackView.axis = .horizontal
    barStackView.distribution = .fillProportionally
    barStackView.preservesSuperviewLayoutMargins = true
    barStackView.spacing = 2
    barStackView.translatesAutoresizingMaskIntoConstraints = false

    titleLabel.text = localizedString(forKey: "live-view.speed-label", table: "LiveView")
  }

  override func update() {
    let currentSpeedRatio = Double(speed) / 100
    let speedRatioThresholds = [.leastNonzeroMagnitude] + heightRatios

    for (bar, threshold) in zip(barStackView.arrangedSubviews, speedRatioThresholds) {
      let isActivated = isEnabled && currentSpeedRatio >= threshold
      let backgroundColor: UIColor = isActivated ? .white : .clear
      let borderColor: UIColor = isActivated ? .white : .statusDisabled
      bar.backgroundColor = backgroundColor
      bar.layer.borderColor = borderColor.cgColor
    }

    let speedPercentage = NumberFormatter.localizedString(from: currentSpeedRatio, number: .percent)
    statusLabel.setText(speedPercentage, animated: true)

    super.update()
  }
}
