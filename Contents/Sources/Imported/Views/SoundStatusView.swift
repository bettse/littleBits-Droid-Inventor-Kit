import UIKit

@objc(SoundStatusView) final class SoundStatusView: StatusView {
  override func configure() {
    super.configure()
    titleLabel.text = localizedString(forKey: "live-view.sound-label", table: "LiveView")
  }
}
