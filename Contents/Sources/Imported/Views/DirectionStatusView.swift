import UIKit

@objc(DirectionStatusView) final class DirectionStatusView: StatusView {
  enum Status {
    case stopped
    case movingForward
    case movingBackward
  }

  private let backArrow = UIImageView(image: #imageLiteral(resourceName: "backward_arrow"))
  private let forwardArrow = UIImageView(image: #imageLiteral(resourceName: "forward_arrow"))

  var status: Status = .stopped {
    didSet {
      update()
    }
  }

  override func layout() {
    super.layout()

    backArrow.installImageAspectRatioConstraint()
    backArrow.translatesAutoresizingMaskIntoConstraints = false
    contentView.addSubview(backArrow)

    forwardArrow.installImageAspectRatioConstraint()
    forwardArrow.translatesAutoresizingMaskIntoConstraints = false
    contentView.addSubview(forwardArrow)

    let views = [
      "back": backArrow,
      "center": contentCenterGuide,
      "forward": forwardArrow,
    ]

    let row = NSLayoutConstraint.constraints(
      withVisualFormat: "|-(>=0)-[back]-(4)-[center]-(4)-[forward]-(>=0)-|",
      options: .alignAllCenterY, metrics: nil, views: views)

    let backColumn = NSLayoutConstraint.constraints(
      withVisualFormat: "V:|-(>=0)-[back]-(>=0)-|", metrics: nil, views: views)

    let forwardColumn = NSLayoutConstraint.constraints(
      withVisualFormat: "V:|-(>=0)-[forward]-(>=0)-|", metrics: nil, views: views)

    NSLayoutConstraint.activate(row)
    NSLayoutConstraint.activate(backColumn)
    NSLayoutConstraint.activate(forwardColumn)
  }

  override func configure() {
    super.configure()
    titleLabel.text = localizedString(forKey: "live-view.direction-label", table: "LiveView")
  }

  override func update() {
    backArrow.tintColor = .statusDisabled
    forwardArrow.tintColor = .statusDisabled

    let description: String

    switch status {
    case .stopped:
      description = localizedString(forKey: "live-view.direction.stopped", table: "LiveView")
    case .movingForward:
      forwardArrow.setTintColor(.white, animated: true)
      description = localizedString(forKey: "live-view.direction.forward", table: "LiveView")
    case .movingBackward:
      backArrow.setTintColor(.white, animated: true)
      description = localizedString(forKey: "live-view.direction.backward", table: "LiveView")
    }

    statusLabel.setText(description, animated: true)

    super.update()
  }
}
