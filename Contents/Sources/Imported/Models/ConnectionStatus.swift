public enum ConnectionStatus: String, CustomStringConvertible {
  public static let allValues: [ConnectionStatus] = [
    .connected,
    .connectionFailed,
    .connectionTimedOut,
    .disconnected,
    .startedSearching
  ]

  case connected
  case connectionFailed
  case connectionTimedOut
  case disconnected
  case startedSearching

  public var description: String {
    switch self {
    case .connected:
      return "Connected"
    case .connectionFailed:
      return "Unable to connect"
    case .connectionTimedOut:
      return "Timed out while searching for a device"
    case .disconnected:
      return "Disconnected"
    case .startedSearching:
      return "Searching"
    }
  }

  public var name: String {
    return self.rawValue
  }
}
