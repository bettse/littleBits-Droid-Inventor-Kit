public enum TurnDirection: String {
  case left
  case right
  case center
}

extension TurnDirection {
  public var movement: MovePreset {
    switch self {
    case .left:
      return .forwardLeft
    case .right:
      return .forwardRight
    case .center:
      return .forward
    }
  }
}
