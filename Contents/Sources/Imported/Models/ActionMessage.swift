import Foundation

public enum ActionMessage {
  case connect
  case disconnect
  case moveForward(speed: Int, duration: TimeInterval)
  case moveBackward(speed: Int, duration: TimeInterval)
  case playSound(SoundPreset)
  case setTurnDirection(TurnDirection, angle: Int)
  case stop

  public var name: String {
    switch self {
    case .connect:
      return "connect"
    case .disconnect:
      return "disconnect"
    case .moveForward:
      return "moveForward"
    case .moveBackward:
      return "moveBackward"
    case .playSound:
      return "playSound"
    case .setTurnDirection:
      return "setTurnDirection"
    case .stop:
      return "stop"
    }
  }
}
