import CoreBluetooth

protocol DroidControllerDelegate: class {
  func droidControllerDidTimeoutScan()
  func droidControllerDidFail(forState state: CBManagerState)
  func droidControllerDidFind(device: BLEDevice)
  func droidControllerDidDisconnect()
}

public class DroidController: NSObject {
  let serviceUUID: CBUUID
  let characteristicUUID: CBUUID
  var scanTimeoutTimer: Timer?
  var connectedPeripheral: CBPeripheral?
  var connectedDevice: BLEDevice?
  weak var delegate: DroidControllerDelegate?

  public init(serviceUUID: CBUUID, characteristicUUID: CBUUID) {
    self.serviceUUID = serviceUUID
    self.characteristicUUID = characteristicUUID
  }

  func send(_ request: BLERequest) {
    guard let connectedDevice = connectedDevice else { return }
    connectedDevice.send(data: request.encode())
  }
}
