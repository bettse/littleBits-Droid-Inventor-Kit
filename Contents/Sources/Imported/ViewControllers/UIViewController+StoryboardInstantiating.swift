import UIKit

extension NSObjectProtocol where Self: UIViewController {
  public static func instantiate(from storyboard: UIStoryboard) -> Self {
    let className = String(describing: self)
    let viewController = storyboard.instantiateViewController(withIdentifier: className)
    return viewController as! Self
  }
}
