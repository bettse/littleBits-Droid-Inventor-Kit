import CoreBluetooth
import UIKit

@objc(ConnectionViewController) public final class ConnectionViewController: UIViewController {
  public struct StatusIndicators: OptionSet {
    public let rawValue: Int

    public init(rawValue: Int) {
      self.rawValue = rawValue
    }

    public static let direction = StatusIndicators(rawValue: 1 << 0)
    public static let sound = StatusIndicators(rawValue: 1 << 1)
    public static let speed = StatusIndicators(rawValue: 1 << 2)
    public static let turnAngle = StatusIndicators(rawValue: 1 << 3)

    public static let none = StatusIndicators()
    public static let all: StatusIndicators = [.direction, .sound, .speed, .turnAngle]
  }

  @IBOutlet private(set) var connectionViewContainer: UIView!
  @IBOutlet private(set) var disconnectedViewContainer: UIView!

  @IBOutlet private(set) var disconnectedView: UIView!
  @IBOutlet private(set) var droidImageView: UIImageView!
  @IBOutlet private(set) var logoImageView: UIImageView!

  @IBOutlet private(set) var disconnectedDetailLabel: UILabel!
  @IBOutlet private(set) var disconnectedTitleLabel: UILabel!
  @IBOutlet private(set) var droidDescriptionLabel: UILabel!
  @IBOutlet private(set) var titleLabel: UILabel!

  @IBOutlet private(set) var directionStatusView: DirectionStatusView!
  @IBOutlet private(set) var speedStatusView: SpeedStatusView!
  @IBOutlet private(set) var turnStatusView: TurnStatusView!
  @IBOutlet private(set) var soundStatusView: SoundStatusView!

  public var droidController: DroidController!
  public var enabledStatusIndicators: StatusIndicators = .all

  private var lastAction: ActionMessage?
  fileprivate var isConnected = false

  public override func viewDidLoad() {
    super.viewDidLoad()

    disconnectedDetailLabel.text = localizedString(forKey: "live-view.disconnected.detail", table: "LiveView")
    disconnectedTitleLabel.text = localizedString(forKey: "live-view.disconnected.title", table: "LiveView")

    droidDescriptionLabel.text = localizedString(forKey: "live-view.droid-description", table: "LiveView")
    droidDescriptionLabel.addCustomShadow()
    titleLabel.text = localizedString(forKey: "live-view.title", table: "LiveView")
    titleLabel.addCustomShadow()

    // TODO: Unhide sound status view once it's implemented
    soundStatusView.isHidden = true

    droidController.delegate = self
    droidImageView.image = #imageLiteral(resourceName: "fpo_r_2_unit_illustration")
    logoImageView.image = #imageLiteral(resourceName: "logo_lock")

    disconnectedView.embed(in: disconnectedViewContainer)

    update(animated: false)
  }

  func receiveAction(_ action: ActionMessage, completionHandler completion: @escaping () -> Void) {
    let moveDuration: Double?

    switch action {
    case .connect, .disconnect:
      return
    case let .moveForward(speed, duration):
      moveDuration = duration
      droidController.send(.makeDCMoveRequest(movement: .forward, speed: speed))
    case let .moveBackward(speed, duration):
      moveDuration = duration
      droidController.send(.makeDCMoveRequest(movement: .backward, speed: speed))
    case let .playSound(sound):
      moveDuration = 2
      droidController.send(.makeSoundRequest(sound))
    case let .setTurnDirection(turnDirection, angle):
      moveDuration = nil
      droidController.send(.makeServoMoveRequest(movement: turnDirection.movement, angle: angle))
    case .stop:
      moveDuration = nil
      droidController.send(.makeDCMoveRequest(movement: .stop))
    }

    lastAction = action
    update(animated: true)

    var deadline = DispatchTime.now()

    if let moveDuration = moveDuration {
      deadline = deadline + moveDuration
    }

    DispatchQueue.main.asyncAfter(deadline: deadline, execute: completion)
  }

  func receiveStatus(_ status: ConnectionStatus) {
  }

  fileprivate func update(animated: Bool) {
    if let action = lastAction {
      update(with: action, animated: animated)
    } else {
      update(with: .stop, animated: animated)
      update(with: .setTurnDirection(.center, angle: 0), animated: animated)
    }

    directionStatusView.activeState = activeState(for: .direction)
    speedStatusView.activeState = activeState(for: .speed)
    turnStatusView.activeState = activeState(for: .turnAngle)

    UIView.animateIf(animated, withDuration: 0.3) { [isConnected] in
      self.droidImageView.alpha = isConnected ? 1 : 0
      self.disconnectedView.alpha = isConnected ? 0 : 1
    }
  }

  private func activeState(for statusIndicator: StatusIndicators) -> StatusView.ActiveState {
    return .enabled
  }

  private func update(with action: ActionMessage, animated: Bool) {
    switch action {
    case .stop:
      directionStatusView.status = .stopped
      speedStatusView.speed = 0

    case let .moveForward(speed, _):
      directionStatusView.status = .movingForward
      speedStatusView.speed = speed

    case let .moveBackward(speed, _):
      directionStatusView.status = .movingBackward
      speedStatusView.speed = speed

    case let .setTurnDirection(direction, angleInDegrees):
      switch direction {
      case .center:
        turnStatusView.status = .centered
      case .left:
        turnStatusView.status = .turningLeft(angle: angleInDegrees)
      case .right:
        turnStatusView.status = .turningRight(angle: angleInDegrees)
      }

    case .connect, .disconnect, .playSound:
      break
    }
  }
}

extension ConnectionViewController: DroidControllerDelegate {
  func droidControllerDidDisconnect() {
    isConnected = false
    DispatchQueue.main.async { self.update(animated: true) }
  }

  func droidControllerDidFind(device: BLEDevice) {
    isConnected = true
    DispatchQueue.main.async { self.update(animated: true) }
  }

  func droidControllerDidFail(forState state: CBManagerState) {
    isConnected = false
    DispatchQueue.main.async { self.update(animated: true) }
  }

  func droidControllerDidTimeoutScan() {
    isConnected = false
    DispatchQueue.main.async { self.update(animated: true) }
  }
}
