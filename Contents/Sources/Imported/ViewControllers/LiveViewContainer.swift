import UIKit

@objc(LiveViewContainer) public final class LiveViewContainer: UIViewController {
  @IBOutlet private(set) var contentView: UIView!

  public var connectionViewController: ConnectionViewController! {
    return childViewControllers.first as! ConnectionViewController?
  }

  public var droidController: DroidController! {
    didSet {
      connectionViewController?.droidController = droidController
    }
  }

  public var enabledStatusIndicators: ConnectionViewController.StatusIndicators = .all {
    didSet {
      connectionViewController?.enabledStatusIndicators = enabledStatusIndicators
    }
  }

  public override func loadView() {
    super.loadView()
    registerFonts()
    performSegue(withIdentifier: "embed", sender: self)
  }

  private func registerFonts() {
    let bundle = Bundle(for: ConnectionViewController.self)
    let fontURLs = bundle.urls(forResourcesWithExtension: "otf", subdirectory: nil) ?? []
    for case let fontURL as CFURL in fontURLs {
      CTFontManagerRegisterFontsForURL(fontURL, .process, nil)
    }
  }

  public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    guard segue.identifier == "embed" else { return }
    let destination = segue.destination as! ConnectionViewController
    destination.droidController = droidController
    destination.enabledStatusIndicators = enabledStatusIndicators
  }

  public override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()
    overrideTraitCollection(for: view.bounds.size)
  }

  public override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    super.viewWillTransition(to: size, with: coordinator)
    overrideTraitCollection(for: size)
  }

  private func overrideTraitCollection(for size: CGSize) {
    var traits = traitCollection

    switch size.orientation {
    case .portrait:
      let compactWidth = UITraitCollection(horizontalSizeClass: .compact)
      traits = UITraitCollection(traitsFrom: [traits, compactWidth])
    case .landscape:
      let compactHeight = UITraitCollection(verticalSizeClass: .compact)
      traits = UITraitCollection(traitsFrom: [traits, compactHeight])
    case .square:
      break
    }

    setOverrideTraitCollection(traits, forChildViewController: connectionViewController)
  }
}
