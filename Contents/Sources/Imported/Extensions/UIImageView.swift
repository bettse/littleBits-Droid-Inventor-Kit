import UIKit

extension UIImageView {
  @nonobjc func installImageAspectRatioConstraint() {
    assert(image != nil, "expected an image")
    let intrinsicSize = intrinsicContentSize
    let ratio = intrinsicSize.width / intrinsicSize.height
    widthAnchor.constraint(equalTo: heightAnchor, multiplier: ratio).isActive = true
  }
}
