import func Darwin.C.round

extension MovePreset {
  public static let maxAngle = 45
  public static let maxSpeed = 100

  func scaledDCOutputValue(withSpeed speed: Int) -> UInt8 {
    guard self != .stop else { return dcOutputValue }
    let speed = speed.clamped(to: 0...MovePreset.maxSpeed)
    return dcOutputValue.scaled(
      by: speed / MovePreset.maxSpeed,
      relativeTo: MovePreset.stop.dcOutputValue)
  }

  func scaledServoOutputValue(withAngle angle: Int) -> UInt8 {
    guard ![.stop, .forward, .backward].contains(self) else { return servoOutputValue }
    let angle = angle.clamped(to: 0...MovePreset.maxAngle)
    return servoOutputValue.scaled(
      by: angle / MovePreset.maxAngle,
      relativeTo: MovePreset.stop.servoOutputValue)
  }
}

private extension UInt8 {
  func scaled(by fraction: Fraction, relativeTo pivot: UInt8) -> UInt8 {
    let pivot = Int(pivot)
    let translated = Int(self) - pivot
    let scaled = Double(translated) * fraction.percentage
    let untranslated = Int(round(scaled)) + pivot
    return UInt8(untranslated)
  }
}

private struct Fraction {
  var numerator: Int
  var denominator: Int

  var percentage: Double {
    return Double(numerator) / Double(denominator)
  }
}

private func / (lhs: Int, rhs: Int) -> Fraction {
  return Fraction(numerator: lhs, denominator: rhs)
}
