extension BLERequest {
  public static func makeDCMoveRequest(movement: MovePreset, speed: Int) -> BLERequest {
    return makeDCMoveRequest(value: movement.scaledDCOutputValue(withSpeed: speed))
  }

  public static func makeDCMoveRequest(movement: MovePreset) -> BLERequest {
    return makeDCMoveRequest(value: movement.dcOutputValue)
  }

  public static func makeServoMoveRequest(movement: MovePreset, angle: Int) -> BLERequest {
    return makeServoMoveRequest(value: movement.scaledServoOutputValue(withAngle: angle))
  }

  public static func makeServoMoveRequest(movement: MovePreset) -> BLERequest {
    return makeServoMoveRequest(value: movement.servoOutputValue)
  }

  public static func makeSoundRequest(_ sound: SoundPreset) -> BLERequest {
    return BLERequest(command: .CMD_PLAY_SFX, payload: [sound.rawValue])
  }
}

// MARK: - BLERequests from Raw Values
private extension BLERequest {
  static func makeDCMoveRequest(value: UInt8) -> BLERequest {
    let output = Control.Output.out3
    return BLERequest(command: .CMD_SET_BITSNAP, payload: [output.output, value])
  }

  static func makeServoMoveRequest(value: UInt8) -> BLERequest {
    let output = Control.Output.out2
    return BLERequest(command: .CMD_SET_BITSNAP, payload: [output.output, value])
  }
}
