import UIKit

extension UIFont {
  @nonobjc static let statusDescription = UIFont(name: "OCRAStd", size: 12)!
  @nonobjc static let statusHeader = UIFont(name: "Eund-Bold", size: 10)!
}
