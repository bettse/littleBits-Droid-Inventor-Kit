import UIKit

extension UIColor {
  @nonobjc static let shadowPrimary = #colorLiteral(red: 0.5058823529, green: 0.7607843137, blue: 0.8784313725, alpha: 1)
  @nonobjc static let shadowSecondary = #colorLiteral(red: 0.4039215686, green: 0.1058823529, blue: 0.1803921569, alpha: 1)
  @nonobjc static let statusActive = #colorLiteral(red: 1, green: 0.3529411765, blue: 0, alpha: 1)
  @nonobjc static let statusEnabled = #colorLiteral(red: 0, green: 0.8980392157, blue: 1, alpha: 1)
  @nonobjc static let statusDisabled = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.2)
}
