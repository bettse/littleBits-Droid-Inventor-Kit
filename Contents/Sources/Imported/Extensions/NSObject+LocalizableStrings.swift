import UIKit

extension NSObjectProtocol {
  func localizedString(forKey key: String, table: String? = nil) -> String {
    let defaultValue = Bundle(for: type(of: self)).localizedString(forKey: key, value: nil, table: nil)
    return Bundle.main.localizedString(forKey: key, value: defaultValue, table: table)
  }
}
