import UIKit

extension UIView {
  @nonobjc func setTintColor(_ tintColor: UIColor!, animated: Bool) {
    let crossFade = CATransition()
    crossFade.duration = 0.3
    crossFade.type = kCATransitionFade
    layer.add(crossFade, forKey: nil)

    self.tintColor = tintColor
  }
}
