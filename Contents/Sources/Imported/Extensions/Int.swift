extension Int {
  func clamped(to range: CountableClosedRange<Int>) -> Int {
    if self < range.lowerBound { return range.lowerBound }
    if self > range.upperBound { return range.upperBound }
    return self
  }
}
