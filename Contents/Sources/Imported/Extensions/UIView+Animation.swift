import UIKit

extension UIView {
  static func animateIf(_ shouldAnimate: Bool, withDuration duration: TimeInterval, animations: @escaping () -> Void) {
    if !shouldAnimate { setAnimationsEnabled(false) }
    animate(withDuration: duration, animations: animations)
    if !shouldAnimate { setAnimationsEnabled(true) }
  }
}
