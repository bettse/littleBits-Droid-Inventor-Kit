import Foundation

extension NumberFormatter {
  @nonobjc static func localizedString(from floatingPoint: Double, number style: NumberFormatter.Style) -> String {
    let number = NSNumber(value: floatingPoint)
    return localizedString(from: number, number: style)
  }
}
