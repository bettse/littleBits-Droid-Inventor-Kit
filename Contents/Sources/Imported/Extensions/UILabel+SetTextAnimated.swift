import UIKit

extension UILabel {
  public func setText(_ text: String, animated: Bool) {
    if !animated {
      UIView.setAnimationsEnabled(false)
    }

    UIView.transition(with: self, duration: 0.3, options: [.transitionCrossDissolve], animations: {
      self.text = text
    }, completion: nil)

    if !animated {
      UIView.setAnimationsEnabled(true)
    }
  }
}
