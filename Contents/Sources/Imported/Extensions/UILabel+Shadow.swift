import UIKit

extension UILabel {
  func addCustomShadow() {
    let shadow = NSShadow()
    shadow.shadowColor = UIColor.shadowPrimary
    shadow.shadowOffset = CGSize(width: -1, height: 0)

    let text = self.text ?? ""
    let attributed = NSMutableAttributedString(string: text)
    attributed.addAttribute(NSShadowAttributeName, value: shadow, range: NSRange(0..<text.utf16.count))
    attributedText = attributed

    layer.shadowColor = UIColor.shadowSecondary.cgColor
    layer.shadowOpacity = 1
    layer.shadowOffset = CGSize(width: -2, height: 0)
    layer.shadowRadius = 0
  }
}
