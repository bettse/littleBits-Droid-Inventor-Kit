import UIKit

extension CGSize {
  enum Orientation {
    case portrait
    case landscape
    case square
  }

  var orientation: Orientation {
    if width < height {
      return .portrait
    } else if width == height {
      return .square
    } else {
      return .landscape
    }
  }
}
