import UIKit

extension UIView {
  func clearOverriddenTintColors() {
    hierarchy.forEach { $0.tintColor = nil }
  }

  /// Performs a breadth-first search of the view hierarchy,
  /// starting from the receiver.
  var hierarchy: AnySequence<UIView> {
    return AnySequence { () -> AnyIterator<UIView> in
      var queue = [self]
      return AnyIterator {
        if let next = queue.first {
          queue.removeFirst()
          queue.append(contentsOf: next.subviews)
          return next
        } else {
          return nil
        }
      }
    }
  }
}
