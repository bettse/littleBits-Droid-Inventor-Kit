import UIKit

extension UIView {
  func embed(in view: UIView) {
    translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(self)
    pinEdges(to: view)
  }
  
  func pinEdges(to view: UIView) {
    NSLayoutConstraint.activate([
      topAnchor.constraint(equalTo: view.topAnchor),
      leadingAnchor.constraint(equalTo: view.leadingAnchor),
      bottomAnchor.constraint(equalTo: view.bottomAnchor),
      trailingAnchor.constraint(equalTo: view.trailingAnchor),
    ])
  }
}
