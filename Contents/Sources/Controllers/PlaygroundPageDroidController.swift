import Foundation
import PlaygroundSupport

public final class PlaygroundPageDroidController: PlaygroundRemoteLiveViewProxyDelegate {
  public static let shared = PlaygroundPageDroidController(
    proxy: PlaygroundPage.current.liveView as! PlaygroundRemoteLiveViewProxy)

  private var lastStatus: ConnectionStatus?
  private let proxy: PlaygroundRemoteLiveViewProxy
  private var responseHandler: ((ActionMessageResponse) -> Void)?

  init(proxy: PlaygroundRemoteLiveViewProxy) {
    self.proxy = proxy
    proxy.delegate = self
  }

  public var connectionStatus: ConnectionStatus {
    return lastStatus!
  }

  public func send(_ action: ActionMessage, responseHandler: @escaping (ActionMessageResponse) -> Void) {
    self.responseHandler = responseHandler
    proxy.send(action.playgroundValue)
  }

  public func remoteLiveViewProxy(_ remoteLiveViewProxy: PlaygroundRemoteLiveViewProxy, received message: PlaygroundValue) {
    if let status = ConnectionStatus(playgroundValue: message) {
      lastStatus = status
    } else if let response = ActionMessageResponse(playgroundValue: message) {
      responseHandler?(response)
      if case .complete = response {
        responseHandler = nil
      }
    }
  }

  public func remoteLiveViewProxyConnectionClosed(_ remoteLiveViewProxy: PlaygroundRemoteLiveViewProxy) {
  }
}
