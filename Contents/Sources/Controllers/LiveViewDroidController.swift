import CoreBluetooth
import Foundation
import PlaygroundBluetooth

public final class LiveViewDroidController: DroidController {
  let central: PlaygroundBluetoothCentralManager

  public init() {
    let starServiceUUID = CBUUID(string: "D9D9E9E0-AA4E-4797-8151-CB41CEDAF2AD")
    let dataCharacteristicUUID = CBUUID(string: "D9D9E9E1-AA4E-4797-8151-CB41CEDAF2AD")
    let queue = DispatchQueue(label: "cc.littlebits.discovery")
    central = PlaygroundBluetoothCentralManager(services: [starServiceUUID], queue: queue)
    super.init(serviceUUID: starServiceUUID, characteristicUUID: dataCharacteristicUUID)
    central.delegate = self
  }

  deinit {
    connectedDevice?.disconnect()
  }
}

extension LiveViewDroidController: PlaygroundBluetoothCentralManagerDelegate {
  public func centralManagerStateDidChange(_ centralManager: PlaygroundBluetoothCentralManager) {
    switch central.state {
    case .poweredOn: central.connectToLastConnectedPeripheral()
    default: delegate?.droidControllerDidFail(forState: central.state)
    }
  }

  public func centralManager(_ centralManager: PlaygroundBluetoothCentralManager, didConnectTo peripheral: CBPeripheral) {
    connectedPeripheral = peripheral
    peripheral.delegate = self
    peripheral.discoverServices([serviceUUID])
  }

  public func centralManager(_ centralManager: PlaygroundBluetoothCentralManager, didFailToConnectTo peripheral: CBPeripheral, error: Error?) {
    print(error ?? "Failed to connect to peripheral")
  }

  public func centralManager(_ centralManager: PlaygroundBluetoothCentralManager, didDisconnectFrom peripheral: CBPeripheral, error: Error?) {
    connectedPeripheral = .none
    connectedDevice = .none
    delegate?.droidControllerDidDisconnect()
  }
}

extension LiveViewDroidController: CBPeripheralDelegate {
  public func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
    guard peripheral == connectedPeripheral else { return }
    peripheral.services?.forEach {
      peripheral.discoverCharacteristics([characteristicUUID], for: $0)
    }
  }

  public func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
    let disconnect: () -> Void = { [weak central] in
      central?.disconnect(from: peripheral)
    }

    guard peripheral == connectedPeripheral, service.uuid == serviceUUID else { return }
    guard let characteristic = service.characteristics?.first(where: { $0.uuid == characteristicUUID }) else {
      disconnect()
      return
    }

    peripheral.setNotifyValue(true, for: characteristic)
    connectedDevice = BLEDevice(peripheral: peripheral, characteristic: characteristic, disconnectHandler: disconnect)

    // FIXME: Resetting the device voltage upon connection prevents sending commands.
//    var resetRequests = BLERequest.voltageResetRequests
//    resetRequests.forEach { send($0) }
    let ledResetRequest = BLERequest.makeLedColorRequest(color: .white)
    let servoResetRequest = BLERequest.makeServoMoveRequest(movement: .stop)
    let dcResetRequest = BLERequest.makeDCMoveRequest(movement: .stop)

    [ledResetRequest, servoResetRequest, dcResetRequest].forEach { send($0) }

    delegate?.droidControllerDidFind(device: connectedDevice!)
  }

  public func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
    connectedDevice?.setValue(characteristic.value ?? Data())
  }

  public func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
    connectedDevice?.didWriteValue()
  }
}

