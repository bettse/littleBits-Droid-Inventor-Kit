import PlaygroundSupport

extension LiveViewContainer: PlaygroundLiveViewMessageHandler {
  public func receive(_ message: PlaygroundValue) {
    if let message = ActionMessage(playgroundValue: message) {
      connectionViewController.receiveAction(message, completionHandler: complete)
    }
  }

  private func complete() {
    let response = ActionMessageResponse.complete
    send(response.playgroundValue)
  }
}
