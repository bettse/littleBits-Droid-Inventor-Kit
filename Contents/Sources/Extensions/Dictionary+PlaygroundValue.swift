import Foundation
import PlaygroundSupport

extension Dictionary where Value == PlaygroundValue {
  mutating func parseData(forKey key: Key) -> Data? {
    guard case let .data(value)? = self[key] else { return nil }
    self[key] = nil
    return value
  }

  mutating func parseDictionary(forKey key: Key) -> [String: PlaygroundValue]? {
    guard case let .dictionary(value)? = self[key] else { return nil }
    self[key] = nil
    return value
  }

  mutating func parseDouble(forKey key: Key) -> Double? {
    guard case let .floatingPoint(value)? = self[key] else { return nil }
    self[key] = nil
    return value
  }

  mutating func parseInt(forKey key: Key) -> Int? {
    guard case let .integer(value)? = self[key] else { return nil }
    self[key] = nil
    return value
  }

  mutating func parseString(forKey key: Key) -> String? {
    guard case let .string(value)? = self[key] else { return nil }
    self[key] = nil
    return value
  }
}
