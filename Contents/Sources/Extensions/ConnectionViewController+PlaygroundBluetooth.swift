import CoreBluetooth
import PlaygroundBluetooth
import UIKit

extension ConnectionViewController: PlaygroundBluetoothConnectionViewDataSource {
  public func connectionView(_ connectionView: PlaygroundBluetoothConnectionView, itemForPeripheral peripheral: CBPeripheral, withAdvertisementData advertisementData: [String : Any]?) -> PlaygroundBluetoothConnectionView.Item {
    // Provide display information associated with a peripheral item.
    let name = peripheral.name ?? "Unknown Device"
    let icon = #imageLiteral(resourceName: "droid_inventor_icon")
    let issueIcon = icon
    return PlaygroundBluetoothConnectionView.Item(name: name, icon: icon, issueIcon: issueIcon, firmwareStatus: nil, batteryLevel: nil)
  }
}

extension ConnectionViewController: PlaygroundBluetoothConnectionViewDelegate {
  public func connectionView(_ connectionView: PlaygroundBluetoothConnectionView, shouldDisplayDiscovered peripheral: CBPeripheral, withAdvertisementData advertisementData: [String : Any]?, rssi: Double) -> Bool {
    return rssi >= -70
  }

  public func connectionView(_ connectionView: PlaygroundBluetoothConnectionView, titleFor state: PlaygroundBluetoothConnectionView.State) -> String {
    switch state {
    case .noConnection:
      return NSLocalizedString("shared.connection.noConnection",
        comment: "Connection button label when no device is connected.")

    case .connecting:
      return NSLocalizedString("shared.connection.connecting",
        comment: "Connection button label while connecting.")

    case .searchingForPeripherals:
      return NSLocalizedString("shared.connection.searchingForPeripherals",
        comment: "Connection button label while searching.")

    case .selectingPeripherals:
      return NSLocalizedString("shared.connection.selectingPeripherals",
        comment: "Connection button label while selecting droid.")

    case .connectedPeripheralFirmwareOutOfDate:
      return NSLocalizedString("shared.connection.connectedPeripheralFirmwareOutOfDate",
        comment: "Connection button label when firmware out of date.")
    }
  }

  public func connectionView(_ connectionView: PlaygroundBluetoothConnectionView, firmwareUpdateInstructionsFor peripheral: CBPeripheral) -> String {
    // Provide firmware update instructions.
    return "Firmware update instructions here."
  }
}
