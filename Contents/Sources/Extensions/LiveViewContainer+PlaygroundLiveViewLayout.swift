import PlaygroundSupport
import UIKit

extension LiveViewContainer: PlaygroundLiveViewSafeAreaContainer {
  public func installSafeAreaLayoutConstraints() {
    contentView.topAnchor.constraint(equalTo: liveViewSafeAreaGuide.topAnchor).isActive = true
  }
}
