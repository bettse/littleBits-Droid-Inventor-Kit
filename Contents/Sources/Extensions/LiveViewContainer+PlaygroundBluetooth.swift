import PlaygroundBluetooth
import UIKit

extension LiveViewContainer {
  public func installConnectionView(for droidController: LiveViewDroidController) {
    loadViewIfNeeded()

    let connectionView = PlaygroundBluetoothConnectionView(centralManager: droidController.central)
    connectionView.delegate = connectionViewController
    connectionView.dataSource = connectionViewController

    let connectionViewContainer = connectionViewController.connectionViewContainer!
    connectionViewContainer.addSubview(connectionView)
    NSLayoutConstraint.activate([
      connectionView.topAnchor.constraint(equalTo: connectionViewContainer.topAnchor),
      connectionView.trailingAnchor.constraint(equalTo: connectionViewContainer.trailingAnchor),
      connectionView.bottomAnchor.constraint(equalTo: connectionViewContainer.bottomAnchor),
      connectionView.leadingAnchor.constraint(equalTo: connectionViewContainer.leadingAnchor),
    ])
  }
}
