import PlaygroundSupport

extension ConnectionStatus {
  init?(playgroundValue: PlaygroundValue) {
    guard case var .dictionary(playgroundValue) = playgroundValue,
      case "connection_status"? = playgroundValue.parseString(forKey: "type"),
      let name = playgroundValue.parseString(forKey: "name")
      else { return nil }

    self.init(rawValue: name)
  }

  var playgroundValue: PlaygroundValue {
    return .dictionary([
      "type": .string("connection_status"),
      "name": .string(name),
    ])
  }
}
