import Foundation
import PlaygroundSupport

extension ActionMessage {
  init?(playgroundValue: PlaygroundValue) {
    guard case var .dictionary(playgroundValue) = playgroundValue,
      case "action"? = playgroundValue.parseString(forKey: "type"),
      let name = playgroundValue.parseString(forKey: "name")
      else { return nil }

    switch name {
    case "connect":
      self = .connect
    case "disconnect":
      self = .disconnect
    case "stop":
      self = .stop

    case "moveForward":
      guard let speed = playgroundValue.parseInt(forKey: "speed"),
        let duration = playgroundValue.parseDouble(forKey: "duration")
        else { return nil }
      self = .moveForward(speed: speed, duration: duration)
    case "moveBackward":
      guard let speed = playgroundValue.parseInt(forKey: "speed"),
        let duration = playgroundValue.parseDouble(forKey: "duration")
        else { return nil }
      self = .moveBackward(speed: speed, duration: duration)

    case "playSound":
      guard let data = playgroundValue.parseData(forKey: "sound"),
        let sound = data.first.flatMap(SoundPreset.init(rawValue:))
        else { return nil }
      self = .playSound(sound)

    case "setTurnDirection":
      guard var turn = playgroundValue.parseDictionary(forKey: "turn"),
        let direction = turn.parseString(forKey: "direction").flatMap(TurnDirection.init(rawValue:)),
        let angle = turn.parseInt(forKey: "angle")
        else { return nil }
      self = .setTurnDirection(direction, angle: angle)

    default:
      return nil
    }
  }

  var playgroundValue: PlaygroundValue {
    var dictionary: [String: PlaygroundValue] = [
      "type": .string("action"),
      "name": .string(name),
    ]

    switch self {
    case let .moveForward(speed, duration), let .moveBackward(speed, duration):
      dictionary["speed"] = .integer(speed)
      dictionary["duration"] = .floatingPoint(duration)

    case let .playSound(sound):
      let data = Data([sound.rawValue])
      dictionary["sound"] = .data(data)

    case let .setTurnDirection(direction, angle):
      dictionary["turn"] = .dictionary([
        "direction": .string(direction.rawValue),
        "angle": .integer(angle),
      ])

    case .connect, .disconnect, .stop:
      break
    }

    return .dictionary(dictionary)
  }
}
