import PlaygroundSupport

public enum ActionMessageResponse: String {
  case complete

  init?(playgroundValue: PlaygroundValue) {
    guard case var .dictionary(playgroundValue) = playgroundValue,
      case "action_response"? = playgroundValue.parseString(forKey: "type"),
      let name = playgroundValue.parseString(forKey: "name")
      else { return nil }

    self.init(rawValue: name)
  }

  var playgroundValue: PlaygroundValue {
    return .dictionary([
      "type": .string("action_response"),
      "name": .string(rawValue),
    ])
  }
}
